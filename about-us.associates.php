<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>
<style>
a.link
	{
		color:#999;
		}
a.link:hover
	{
		color:#000;
		}
</style>
<body>


<div id="our-mission" class="about-us-2l neutro">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
		
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
                         	<div class="col-lg-12">
					 			<h1>Associates</h1>
					 			<p>Associates are experts in their fields who have their own consulting practice and who contribute in specific ways to the mission of Coherence. All have multinational experience across a 
					 				variety of industries and practices where they contribute their wealth of knowledge and expertise.</p>
                              	<p>&nbsp;</p>
                            </div>
                            <div class="col-md-12">
                            	<img src="assets/images/roberto_saco.png" alt="Roberto Saco" class="col-lg-4 pull-left ">
							    <h1 style="margin-top:0;">Roberto Saco</h1>
							    <p>Roberto Saco has been immersed in the disciplines of process and change management and service quality for over 20 years. He led the business transformation program for one of the most profitable Divisions at American Express Company. He is an avid collector and promoter of innovative business practices that lead to better organizational outcomes.
								Mr. Saco is a senior consultant at Coherence and a developer of the "Partnership Innovation Sessions" offered through our Workshops and Coaching services. His field of expertise focuses around the themes of service design, business process management, and organizational change & transformation.</p>
								<p>Working with banking, insurance, and IT clients in the Americas, Mr. Saco operates under the guiding principles of dialogue, co-creation, innovation, and sustainability. In addition to his long-term association with professional societies like IIE (Institute of Industrial Engineers) and ASQ (American Society for Quality), he also belongs to the Change Leaders global community of practice.</p>
								<p>Mr. Saco has served as a Senior Examiner for both the Florida Sterling and the USA Baldrige Performance Excellence programs. His involvement with these programs began in 1990, culminating with his appointment to the Sterling Award Judges Panel in 2000. His articles have appeared in diverse publications, such as Quality Progress, Industrial Management, and the Design Management Review. In July 2008 Mr. Saco became the President of the American Society for Quality (ASQ, www.asq.org), a professional association with nearly 95,000 members worldwide.</p>
							 	<p>&nbsp;</p>
                            </div>
                            <div class="col-md-12">
                            	<img src="assets/images/marcelo_salup.png" alt="Marcelo Salup" class="col-lg-4 pull-left">                     
					 		<h1 style="margin-top:0;">Marcelo Salup</h1>
<p>Marcelo supports Coherence clients to envision innovative advertising and branding partnerships that enable client firms to shift customer perceptions and capture market-share and increase top of mind.
Because of his capability to move productively from the creative to the analytical side of business and back again, he is an invaluable resource to help companies interested in partnering to ground their go-to-market strategies based on sound business analytics and creative strategies.
        Marcelo has over 20 years' experience in the marketing, media and advertising industries. From working as a copywriter at Kenyon & Eckhardt in Madrid, early in his career, and later becoming Creative Director. In the US, he earned Addy Awards for his work with the American Dairy Association and for Amoco, turning a simple ad buy into a full sponsorship integrating the founder of the Grand Prix &#8211; Ralph Sanchez &#8211; into one of the Amoco's most successful TV spots.</p>
       <p>&nbsp;</p>
                            </div>
                            <div class="col-md-12">
                             <img src="assets/images/david_drew.png" alt="David Drew" class="col-lg-4 pull-left ">                    
						 		<h1 style="margin-top:0;">David Drew</h1>
								<p>David Drew, has spent nearly two decades planning and executing marketing strategies at international level in a range of industry-leading firms.  David's marketing management experiences in telecommunications, software, automotive, manufacturing, training and management consulting environments have equipped him with a suite of planning and execution tools which have been adapted successfully across industries and functions.  He has executed every aspect of the marketing mix, worked at every level of the marketing function within a number of technology providers, and spent several years spent heading up marketing, alliances and licensing for a specialist global professional services firm.<br>
								As a leadership developer and management consultant, David has worked with clients from the C-suite to operational teams, equipping clients with and coaching them in capabilities from program planning, project management, problem solving, decision making, risk management, human performance management, marketing planning/execution, partnership and alliance management, and others. </p>
								<p>While a consultant in troubleshooting methodologies for Kepner-Tregoe - the premier global troubleshooting consultancy - David led programs for clients such as IBM Rational, Exelon, Perrigo, Pacific Gas & Electric, Johnson & Johnson, BAe Systems, Siemens Diagnostics, Silicon Valley Bank, The Walt Disney Company, Shell and Motorola.<br>
								He has managed various aspects of business operations (including P&L responsibility) in over 30 countries and has lived and worked in the UK, France, Spain and the USA.  David has strong client consulting delivery experience, and top flight experience leading and coaching global and multilingual teams. </p>
								<p>He is fluent in French and Spanish, has sadly forgotten most of his Italian and Catalan, and can play passable bar-room blues guitar.</p>
							 <p>&nbsp;</p>
                            </div>


 
                           </div>
                              
   
                          
				 	
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>