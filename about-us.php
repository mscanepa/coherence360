<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body  id="page2">


<div id="about-us" data-role="page">
	
		<?php include './common/header.php'; ?>
		
		<div class="bd">
			<div class="container">				
				<div class="prev-page"><a href="./" data-transition="slide"  data-direction="reverse" ></a></div>
				<div class="next-page"><a href="framework.php" data-transition="slide"></a></div>
				<div class="row">
					 <div class="col-md-12">
					 	<div class="slideshow ">
					 		<div class="slideshow-inner sttc">				 		
						 		<div class="cita">
						 			<div class="cita-inner">
							 			Only 50%<br>
							 			of Alliances<br>
							 			& Partnerships
							 			succeed<br />
							 			<strong>
								 			WE IMPROVE SUCCESS RATES <bR>UP TO 80%+
								 			
							 			</strong>
							 		</div>
						 		</div>
						 		<div class="desc">
					 				<div class="desc-inner">
					 					<div class="desc-content">
											<p>
												Alliances & Partnerships are proliferating faster than ever, and still only 50% — 
												whether they are alliances, 
												mergers, or joint ventures — succeed.
											</p>
											<p>
											For over 10 years we have established strategic partnerships and alliances for 
											clients, employing another way: a better way. A method of advancing collaborative 
											business practices to improve success rates, and expand opportunities.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
			     	</div>		     	
			    </div><!--/.row-->			    
			</div><!-- container ends-->
		</div>
		  
    
		<?php include './common/footer.php'; ?>
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>