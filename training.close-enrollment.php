<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<style>
a.link
	{
		color:#999;
		}
a.link:hover
	{
		color:#000;
		}
</style>
<body>


<div id="our-mission" class="about-us-2l trainingbg">
	
	<?php include './common/header.php'; ?>
	
<div class="bd">
		<div class="container">
			<div class="mask m1"></div>
		 	<div class="mask m2"></div>
		 	<div class="mask m3"></div>
		 	<div class="mask m4"></div>
		 	<div class="mask m5"></div>
		 	<div class="mask m6"></div>
		 	<div class="mask m7"></div>
		 	<div class="mask m8"></div>
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
                        	<div class="col-md-12">
					 		<h1>Tailored Programs</h1>
                            <p class="short">Tailored Programs are made to fit learning intensives for organizations and 
                            companies in particular industries and applied to specific strategic circumstances.</p>
                            <p class="short">They can be part of Build Capability and may also be used as stand-alone 
                            executive education courses specific to a company or organization.</p>
							<p class="short">
							For more information on these feel free to <a class="link" href="./contact.php">contact us</a>.
							</p>

                          </div>
			 		  </div>
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>