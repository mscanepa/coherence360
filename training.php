<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body id="page4">


<div id="training"  data-role="page">	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">			
			<div class="prev-page"><a href="services.php" data-transition="slide"  data-direction="reverse" ></a></div>
			<div class="next-page"><a href="./" data-transition="slide"></a></div>
			<div class="row">
				 <div class="col-md-12">				 	
				 	<div class="slideshow ">
					 	<div class="slideshow-inner sttc">
					 		<div class="cita">
					 			<div class="cita-inner">
					 				<div class="cita-content">
							 			LEARNING <br>THE "HOW" TO <br><strong>CREATE SUCCESSFUL <br>PARTNERSHIPS<br> & ALLIANCES</strong>

							 		</div>
					 			</div>
					 		</div>
					 		<div class="desc">
					 			<div class="desc-inner">
					 				<div class="desc-content">
										<p>We share our Alliances Framework as both open enrollment and tailored programs, advancing our mission to enable all kinds of companies and organizations to collaborate, for a more resilient economy, a more sustainable future and overall better business relationships.	</p>
									</div>
								</div>						
							</div>
						</div>
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	     </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>   
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>