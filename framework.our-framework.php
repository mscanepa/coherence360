<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="our-framework" class="framework-2l">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner ">
                        <h1 class="multicolum">Alliances Framework</h1>
                        <div class="row colOverview">
							  	<div class="col-sm-12 col-md-6 ">
						 			<p>Our framework comprises two key components: The <strong>5-Territories of 
						 				Alliance Development&#8482;</strong> and the <strong>Operative Partnership 
						 				Methodology&#8482;</strong>. The <strong>5-Territories of Alliance Development&#8482;</strong> is a road-tested
	                            	 business process for establishing successful</p>
	                            </div>
	                            <div class="col-sm-12 col-md-6 ">
	                            	<p> strategic alliances. In each territory, 
	                            	 groups work on a set of business activities that are relevant to where they are in the 
	                            	 process. Each territory naturally transitions to the next through a set of gates and group 
	                            	 leadership challenges that teams must successfully navigate.</p>                           
                            	</div>
                            </div>
                            <div class="col-md-12 corto">
                             <div class="a5territories">
                               
	                               <div class="circle">
			                       		<div class="circle-inner">
			                       			<div class="circle-content">
			                       				ALIGN &<br> PREPARE
			                       			</div>
			                       			<div class="circle-tooltip  tool_1">		                       				
												<p>Align alliance strategy to company strategy, and prepare resources, tools, teams and develop partner approach.
												</p>													                       			
			                       			</div>
			                       		</div>	        	
									</div>
									
									 <div class="circle c2">
			                       		<div class="circle-inner">
			                       			<div class="circle-content">
			                       				INVITE &<br> COMMIT
			                       			</div>
			                       			<div class="circle-tooltip tool_2">		                       				
												<p>Invite potential partner(s) into possible partnership, learn together & commit to developing a Strategic Alliance.
												</p>													                       			
			                       			</div>
			                       		</div>	        	
									</div>
									
									 <div class="circle c3">
			                       		<div class="circle-inner">
			                       			<div class="circle-content">
			                       				CREATE &<br> CONSOLIDATE
			                       			</div>
			                       			<div class="circle-tooltip tool_3">		                       				
												<p>Create possible vision, map potential and consolidate structure into workable alliance Term-Sheet.

												</p>												                       			
			                       			</div>
			                       		</div>	        	
									</div>
									
									<div class="circle c4">
			                       		<div class="circle-inner">
			                       			<div class="circle-content">
			                       				NEGOTIATE <br>& LAUNCH
			                       			</div>
			                       			<div class="circle-tooltip tool_4">		                       				
												<p>Negotiate key contractual elements and launch the Alliance in Market-Place.

												</p>												                       			
			                       			</div>
			                       		</div>	        	
									</div>
									
									<div class="circle c5">
			                       		<div class="circle-inner">
			                       			<div class="circle-content">
			                       				SUSTAIN<br> & DEEPEN
			                       			</div>
			                       			<div class="circle-tooltip tool_5">		                       				
												<p>Sustain partnership through collaboration structure & deepen alliance to increase success.
												</p>												                       			
			                       			</div>
			                       		</div>	        	
									</div>
									
									<h2>5-TERRITORIES OF ALLIANCE DEVELOPMENT&#8482;</h2>
								
                                <p><img src="assets/images/5territorios-arrow.png" alt="5-Territories"></p>
                               </div>
							</div>
                        	<div class="col-md-12 corto">
                             <div class="text-center col-md-6 ">
                             <h2 class="a5territories">Inverted Partnership Cone&#8482;</h2>
                                 <img src="assets/images/cono_invertido.svg" alt="Inverted Partnership Cone" width="80%" >
                               </div>
                            <div class="col-sm-12 col-md-6 ">
                         	<p>&nbsp;</p>
                            	<p>&nbsp;</p>
                                	<p>&nbsp;</p>
                                    	<p>&nbsp;</p>
                                        	<p>&nbsp;</p>
                            <p class="large">The <strong>Operative Partnership Methodology&#8482;</strong> is a system of group coaching, facilitation and dialogue methods that are applied throughout the <strong>5-Territories of Alliance Development&#8482;</strong>. The methodology uses a particular set of tools, group coaching and facilitation practices which support teams to collaborate together and overcome group leadership challenges they face as the alliance is constructed.</p>
                            <p class="large">Together, the <strong>Methodology and the 5-Territories</strong> form a coherent framework, where  the journey taken by alliances teams is intelligently structured using  a clear business process engineered specifically for strategic business collaboration.</p>	
                            </div>
					</div>
                               
				 		</div>
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>