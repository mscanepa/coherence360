<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<style>
a.link
	{
		color:#999;
		}
a.link:hover
	{
		color:#000;
		}
		
ul.resources
	{
		padding-left:15px;}


</style>
<body>


<div id="our-mission" class="framework">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
                       
                       <div class="col-xs-12 col-sm-6 col-md-8">
                       
                        <div class="post">
                        <div  class="col-lg-4 pull-left mCS_img_loaded">
                           <img src="assets/images/borrar1.jpg" width="100%">
                           </div>
                           <h1>To Sell or to Partner? Decisions, Decisions</h1>
                           <h5><a class="link" href="#"><small>Add Comments</small></a></h5>
                           <p>In the "Not so Niche Any More" post we talked about the idea that not all relationships are the same. That some are more strategic and important to a company’s business than others, with some being transactional relationships and others strategic long-term collaborations. These strategic collaborations when well developed lead to collaborative advantage, the key to corporate resilience. But success is not so easy to come by. Executives must decide whether to sell or to partner, and taking the right approach to forming these strategic relationships is key to their success.</p>
                           <p>In the "Not so Niche Any More" post we talked about the idea that not all relationships are the same. That some are more strategic and important to a company’s business than others, with some being transactional relationships and others strategic long-term collaborations. These strategic collaborations when well developed lead to collaborative advantage, the key to corporate resilience. But success is not so easy to come by. Executives must decide whether to sell or to partner, and taking the right approach to forming these strategic relationships is key to their success.</p>
                           <p>In the "Not so Niche Any More" post we talked about the idea that not all relationships are the same. That some are more strategic and important to a company’s business than others, with some being transactional relationships and others strategic long-term collaborations. These strategic collaborations when well developed lead to collaborative advantage, the key to corporate resilience. But success is not so easy to come by. Executives must decide whether to sell or to partner, and taking the right approach to forming these strategic relationships is key to their success.</p>
                           <p>In the "Not so Niche Any More" post we talked about the idea that not all relationships are the same. That some are more strategic and important to a company’s business than others, with some being transactional relationships and others strategic long-term collaborations. These strategic collaborations when well developed lead to collaborative advantage, the key to corporate resilience. But success is not so easy to come by. Executives must decide whether to sell or to partner, and taking the right approach to forming these strategic relationships is key to their success.</p>
                            <h6><a class="link" href="#">Read more...</a></h6>
                        </div>
                        
                        
                      

                       </div>
  						<div class="col-xs-6 col-md-4">
                        <div class="bs-callout bs-callout-warning" id="callout-helper-pull-navbar">
<h4>Resources of the Month</h4>
<ul class="resources">
<li><a class="link" href="assets/pdf/Taking-the-Teeth-Out-of-Team-Traps.pdf" target="_blank">Taking The Teeth out of Team Traps</a></li> 
<li><a class="link" href="assets/pdf/Individual-Reflection-Practice-1.pdf" target="_blank">Individual Leader Reflection Practice 1</a></li>
<li><a class="link" href="assets/pdf/Group-Facilitaiton-Tool.pdf">Group Facilitation Tool</a></li>
<li><a class="link" href="assets/pdf/Michael_Porter_Creating_Shared_Value.pdf" target="_blank">Creating Shared Value Article</a></li></ul>
</div>
                        </div>
                        
                          
                            
                          
				 		</div>
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>