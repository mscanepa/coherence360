<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>

<body >


<div id="our-mission" class="about-us-2l neutro our-approach">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			<div class="mask m1"></div>
		 	<div class="mask m2"></div>
		 	<div class="mask m3"></div>
		 	<div class="mask m4"></div>
		 	<div class="mask m5"></div>
		 	<div class="mask m6"></div>
		 	<div class="mask m7"></div>
		 	<div class="mask m8"></div>
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
                        	<div class="col-md-12">
					 		<h1>Our Approach</h1>
					 		<p class="large">We employ an<em> Applied Coaching & Facilitating Approach</em> in which we partner with select clients, using methods and expertise, and continuously adapting to the industry, and to the particular challenges and opportunities faced by partnering firms.</p>
                            <p class="large">Through this<em> Applied Coaching & Facilitating Approach</em>, we collaborate closely with clients to produce specific, actionable strategies and concrete methods for building alliances and partnerships.</p>
                           <p class="large">This way of working accelerates collaborative learning and continuous adaptation to the realities of a challenging market-place which drove the necessity of collaboration in the first place.</p>
                           <p class="large">Our goal is to help clients develop game-changing alliances and partnerships that become self-correcting, self-generating strategic business relationships. In this way, the alliance can adapt to market changes while also develop new opportunities unseen in its initial launching.</p>
                          
				 		</div>
                       </div>
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>