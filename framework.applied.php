<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>

<body>


<div id="our-mission" class="about-us-2l frameworkbg">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			<div class="mask m1"></div>
		 	<div class="mask m2"></div>
		 	<div class="mask m3"></div>
		 	<div class="mask m4"></div>
		 	<div class="mask m5"></div>
		 	<div class="mask m6"></div>
		 	<div class="mask m7"></div>
		 	<div class="mask m8"></div>
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
					 		<h1>Associates</h1>
					 		<p>
								Associates are experts in their fields who have their own consulting practice and who contribute in specific ways to the mission of Coherence. All have multinational experience across a variety of industries and practices where they bring a wealth of knowledge and expertise.
					 		
					 		</p>
    <h2>Roberto Saco</h2>
    <p>Roberto Saco has been immersed in the disciplines of process and change management and service quality for over 20 years. He led the business transformation program for one of the most profitable Divisions at American Express Company. He is an avid collector and promoter of innovative business practices that lead to better organizational outcomes.
Mr. Saco is a senior consultant at Coherence and a developer of the “Partnership Innovation Sessions” offered through our Workshops and Coaching services. His field of expertise focuses around the themes of service design, business process management, and organizational change & transformation.<br>
Working with banking, insurance, and IT clients in the Americas, Mr. Saco operates under the guiding principles of dialogue, co-creation, innovation, and sustainability. In addition to his long-term association with professional societies like IIE (Institute of Industrial Engineers) and ASQ (American Society for Quality), he also belongs to the Change Leaders global community of practice.<br>
Mr. Saco has served as a Senior Examiner for both the Florida Sterling and the USA Baldrige Performance Excellence programs. His involvement with these programs began in 1990, culminating with his appointment to the Sterling Award Judges Panel in 2000. His articles have appeared in diverse publications, such as Quality Progress, Industrial Management, and the Design Management Review. In July 2008 Mr. Saco became the President of the American Society for Quality (ASQ, www.asq.org), a professional association with nearly 95,000 members worldwide.</p>
<h2>Marcelo Salup</h2>
<p>Marcelo supports Coherence clients to envision innovative advertising and branding partnerships that enable client firms to shift customer perceptions and capture market-share and increase top of mind.
Because of his capability to move productively from the creative to the analytical side of business and back again, he is an invaluable resource to help companies interested in partnering to ground their go-to-
        market strategies based on sound business analytics and creative strategies.
        Marcelo has over 20 years’ experience in the marketing, media and advertising industries. From working as a copywriter at Kenyon & Eckhardt in Madrid, early in his career, and later becoming Creative Director. In the US, he earned Addy Awards for his work with the American Dairy Association and for Amoco, turning a simple ad buy into a full sponsorship integrating the founder of the Grand Prix –Ralph Sanchez—into one of the Amoco’s most successful TV spots.</p>
 <h2>David Drew</h2>
<p>Marcelo supports Coherence clients to envision innovative advertising and branding partnerships that enable client firms to shift customer perceptions and capture market-share and increase top of mind.
Because of his capability to move productively from the creative to the analytical side of business and back again, he is an invaluable resource to help companies interested in partnering to ground their go-to-
        market strategies based on sound business analytics and creative strategies.
        Marcelo has over 20 years’ experience in the marketing, media and advertising industries. From working as a copywriter at Kenyon & Eckhardt in Madrid, early in his career, and later becoming Creative Director. In the US, he earned Addy Awards for his work with the American Dairy Association and for Amoco, turning a simple ad buy into a full sponsorship integrating the founder of the Grand Prix –Ralph Sanchez—into one of the Amoco’s most successful TV spots.</p>
                          
				 		</div>
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>