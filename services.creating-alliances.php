<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="parterships-coaching-facilitation" class="services-2l">	
		<?php include './common/header.php'; ?>
		
		<div class="bd">
			<div class="container">
				<div class="mask m1"></div>
			 	<div class="mask m2"></div>
			 	<div class="mask m3"></div>
			 	<div class="mask m4"></div>
			 	<div class="mask m5"></div>
			 	<div class="mask m6"></div>
				<div class="row">
					 <div class="col-md-12">			 	
					 	<div class="content">
					 		<div class="inner">
						 		<h1>Creating Alliances</h1>
						 		<p class="large">We act as Partnership Developers, consulting to, coaching & facilitating the alliances teams to create and establish the entire alliance, employing our proven methodology and process.                            

						 		</p>
						 		<ul class="large">
							 		<li>Approaching Identified Partners. </li>
									<li>Employing the Alliance Development Process.</li>
									<li>Facilitating partnership collaboration.</li>
									<li>Creating  and consolidating alliance framework and the governance & collaboration structure.</li>
									<li>Negotiating terms & providing launching guidance.</li>
									<li>When the time is right, helping to optimize the alliance or course correct as needed.</li>
						 		</ul>
					 		</div>
						</div>
						<blockquote class="small">
								  	<p class="large">"I would recommend Coherence for their unique knowledge and experience in developing 
								  		strategic partnerships, their innovative approach helped us move beyond the transactional business 
								  		sale to a true partnership environment where opportunities were co-created in a spirit of trust and 
								  		collaboration. Their support was critical in securing several successful partnerships." </p>
									<footer class="large text-right"><cite title="Source Title"><span>Peter Kastanis</span> Director Strategic Alliances Scotiabank International Banking</cite></footer>
								</blockquote>
			     	</div>
			     	
			    </div><!--/.row-->
		    </div><!-- container ends-->
		</div>
		   
		<?php include './common/footer.php'; ?>
    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>