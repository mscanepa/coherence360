<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="parterships-coaching-facilitation" class="services-2l">	
		<?php include './common/header.php'; ?>
		
		<div class="bd">
			<div class="container">
				<div class="mask m1"></div>
			 	<div class="mask m2"></div>
			 	<div class="mask m3"></div>
			 	<div class="mask m4"></div>
			 	<div class="mask m5"></div>
			 	<div class="mask m6"></div>
				<div class="row">
					 <div class="col-md-12">			 	
					 	<div class="content">
					 		<div class="inner">
						 		<h1>Partnerships Facilitation</h1>
						 		<p class="short">Acting as Partnership Development Coach & Facilitator, we help 'course correct' current alliances. </p>
								<p class="short">For new alliances we assist teams to realize the full potential of their collaboration, supporting difficult conversations and helping navigate the challenges that occur in partnership development. In order to achieve this we:
						 		</p>
						 		<ul class="short">
							 		<li>Take the alliances teams through the 5-Territories of Alliance Development&#8482;. </li>
									<li>Build and facilitate made-to-fit Partnership Innovation Sessions. </li>
									<li>Provide tactical facilitated negotiation support.</li>
									<li>Identify challenges the partnership is facing and help uncover and work through them in a productive and effective way. </li>
                                    <li>Course correct by reinvigorating underperforming alliances.</li>
						 		</ul>
						 	</div>					 		
						</div>
			     	</div>
			     	
			    </div><!--/.row-->
		    </div><!-- container ends-->
		</div>
		   
		<?php include './common/footer.php'; ?>
    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>