<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<style>
a.link
	{
		color:#999;
		}
a.link:hover
	{
		color:#000;
		}
</style>
<body>


<div id="training" class="about-us-2l trainingbg">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			<div class="mask m1"></div>
		 	<div class="mask m2"></div>
		 	<div class="mask m3"></div>
		 	<div class="mask m4"></div>
		 	<div class="mask m5"></div>
		 	<div class="mask m6"></div>
		 	<div class="mask m7"></div>
		 	<div class="mask m8"></div>
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
                        	<div class="col-md-12">
					 			<h1>Open Enrollment</h1>
                            		<h2>Facilitating Collaborative Leadership<br>
                            		Enabling Performance in Groups, Teams and Partnerships
                            		</h2>
		                            <p class="large">Collaboration is the new buzzword these days. After all, companies and organizations of all 
		                            	kinds rely on teams collaborating together internally, as well as groups working across organizations on 
		                            	a daily basis to make business a reality. Indeed Collaborative Leadership is the capability that all 
		                            	organizations must master in order for collaborative advantage to take hold and corporate resilience 
		                            	to be developed. But all too often collaboration is condensed down to the individual level and rarely 
		                            	understood also from the small-group perspective where real collaboration takes hold. Indeed, 
		                            	collaboration only occurs at the group level and never from the efforts of one individual. The reality 
		                            	is that collaboration only really happens when groups come-together to work on a shared projects, tasks 
		                            	and goals.</p>
		                            <p class="large">This two day learning intensive introduces participants to the facilitative methods and 
		                            	techniques used to help groups and teams to arrive at a state of continued constructive collaboration. 
		                            	The work is based on small group theory and its application to the organizational context, with real world 
		                            	application.</p>
									<p class="large">To learn more download program brochure <a class="link" href="#">here</a>.</p>
		                             <h2>Strategic Alliances & Partnerships<br>
		                            Building Collaborative Advantage for Growth
		                            </h2>
							 		<p class="large">In today's modern and complex international and domestic marketplace, 
							 			there is no business that succeeds without learning to build and sustain strategic business 
							 			relationships. From for-profit companies to public entities, and not-for-profits, all sorts of 
							 			organizations are partnering to build strategic advantage and bring greater capabilities to market.
							 			 Strategic Alliances and Partnerships are agreements between entities to do just that. In alliances, 
							 			 firms commit resources to achieve complementary and/or supplementary objectives. They are a tool used 
							 			 by executives to arrive at particular strategic goals where companies develop the collaborative 
							 			 leadership capabilities to build collaborative advantage; a competency to build successful alliances
							 			  with: customers, suppliers, competitors, universities, divisions of government, non-for-profits and 
							 			  NGO's. Through Strategic Alliances & Partnerships, companies improve competitive positioning, enable 
							 			  shared value, gain entry to new markets, supplement critical skills, access new capabilities and share 
							 			  the risk or cost of major developments with other firms.</p>

									<p class="large">This two-day learning program is meant to help companies and organizations become better 
										prepared and more informed on how to actually develop Strategic Alliances and Partnerships and then maintain 
										them. The course takes participants through the theoretical aspects of alliances-strategy using case methods 
										and also includes experiential learning components where executive can begin to apply learning directly in 
										their own strategic alliance development activities. The program incorporates a Collaborative Partnership 
										Methodology that provides road-tested methods for facilitating group collaboration so that alliances teams can 
										come together to build the kind of partnership that leads to strategic alliances success. The program is an 
										intensive learning experience that weaves small-group leadership facilitation into an alliances development 
										process that greatly increases your chances of successfully deploying an alliances strategy.</p>
		<p class="large">To learn more download program brochure <a class="link" href="./assets/pdf/StrategicAlliances_Executive-Education-Courses.pdf">here</a>.</p>
                          	</div>
			 		  </div>
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>