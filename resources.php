<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<style>
a.link
	{
		color:#f4f4f4;
		}
a.link:hover
	{
		color:#fff;
		}
		
ul.resources
	{
		padding-left:15px;}
</style>
<body>


<div id="our-mission" class="framework-2l">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
                        <div class="col-xs-6 col-md-4">
                        <div class="bs-callout bs-callout-warning" id="callout-helper-pull-navbar">
<h4>Resources of the Month</h4>
<ul class="resources">
<li><a class="link" href="assets/pdf/Taking-the-Teeth-Out-of-Team-Traps.pdf" target="_blank">Taking The Teeth out of Team Traps</a></li> 
<li><a class="link" href="assets/pdf/Individual-Reflection-Practice-1.pdf" target="_blank">Individual Leader Reflection Practice 1</a></li>
<li><a class="link" href="assets/pdf/Group-Facilitaiton-Tool.pdf">Group Facilitation Tool</a></li>
<li><a class="link" href="assets/pdf/Michael_Porter_Creating_Shared_Value.pdf" target="_blank">Creating Shared Value Article</a></li></ul>
</div>
                        </div>
                       <div class="col-xs-12 col-sm-6 col-md-8">
                       <h1>Resources</h1>
<p class="large">The Alliances Framework developed at Coherence has its roots in the social sciences and strategic business practice. In the social sciences we draw from the seminal works of Enrique Pichon-Rivi&eacute;re and Kurt Lewin, and more contemporary thinkers such as Peter Senge, Bill Isaacs, James Flaherty, Bill Torbert, Don Beck and Ken Wilber, among others. Their works contribute to our understanding and use of Systems Thinking, Dialogue, Integral Coaching, Spiral Dynamics and Integral Theory.</p>
<p  class="large">From Strategic Business Practices we incorporate the work of Peter Drucker, Jordan D. Lewis, and Michael Porter among others.
  Attached are some resources we find useful and supportive to Enable Business Collaboration. We hope you find them useful as you walk your own learning journey.</p>
<h2>Downloadable Resources:</h2>
<p class="large"><strong>Mapping Dialogue</strong> - An interesting study detailing several approaches to participatory decision making. Such varied structures can be used to effectively manage Partnership Development at various points in the process.</p>
<p  class="large"><strong>Mystery of Collective Intelligence</strong>  - An interesting expose into the thought, theory and application of collective leadership. For Partnership Development, the ideas and concepts of group intelligence form the basis for learning how to build alliances throughout the Partnership Journey.</p>
<p  class="large"><strong>Taking the Teeth Out of Team Traps </strong> - Looking at interactions that happen during the alliances development process and discerning the unproductive patterns of communication help alliances teams, through facilitated support, to unravel the knots that can derail a partnership. This is an excellent article that begins to enliven the conversation on the existence of these patterns.</p>
<p  class="large"><strong>Making the Case for Development </strong> - Understanding that different organizations and individuals for that matter function from different stages of cultural development allows an alliances team to discern the problems that may occur throughout the development process and be prepared to build the right structure to address stage oriented conflicts between people and firms.</p>
<p  class="large"><strong>Negotiating with your Nemesis</strong> - An excellent article that bring to light the important of understanding transference and how when relating, executives can inadvertently place their own internal biases on others thinking that they (other people) are the problem, when in fact the problem is closer to home.</p>
<p  class="large"><strong>Creating Shared Value</strong> -  Interesting how Michael Porter has evolved his thinking over the years. This illuminating article speaks to companies and organizations at the Emergent Enterprise Stage of development who catalyze business networks to build new and innovated partnership-based business models.</p>
<p  class="large">The Third State of Alliance Management Study 2009 - ASAP is the premier association for Alliances Development Professionals. In this very informative study from 2009, it's clear that alliances continue to grow at an astounding rate and it's clear to coherence that we are in the age of Collaborative Capitalism. To obtain the most recent version of this annual study you can visit http://www.strategic-alliances.org/.</p>

                       </div>
  						
                        
                          
                            
                          
				 		</div>
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>