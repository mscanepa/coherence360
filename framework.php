<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body id="page3">


<div id="framework" data-role="page">
	
	<?php include './common/header.php'; ?>
	
	<div role="main" class="bd ui-content">
		<div class="container">			
			<div class="prev-page"><a href="about-us.php" data-transition="slide"  data-direction="reverse" ></a></div>
			<div class="next-page"><a href="services.php" data-transition="slide"></a></div>
			<div class="row">				
				 <div class="col-md-12">
				 	<div class="slideshow ">
					 	<div class="slideshow-inner sttc">
					 		<div class="cita">
					 			<div class="cita-inner">
					 				<div class="cita-content">
							 			BY EMPLOYING <br>OUR INTEGRATED FRAMEWORK
							 			
							 			<strong>
								 			<br>WE FACILITATE CREATIVE COLLABORATION		 			
							 			</strong>
							 		</div>
					 			</div>
				 			</div>
				 			<div class="desc">
				 				<div class="desc-inner">
				 					<div class="desc-content">
									<p>Acting as Partnership Development Facilitator & Coach, 
										we apply our Alliances Framework to overcome obstacles and 
										reach of state of true collaboration, where trust is established 
										for long term success.</p>
									<p>Our framework combines a leadership methodology with an alliance development process, 
										bridging a critical gap for sustained success, specifically engineered for 
										building game changing strategic business relationships.</p>
								</div>
								</div>
							</div>						
						</div>	
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>