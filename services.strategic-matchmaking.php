<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="strategic-matchmaking" class="services-2l">	
		<?php include './common/header.php'; ?>
		
		<div class="bd">
			<div class="container">
				<div class="mask m1"></div>
			 	<div class="mask m2"></div>
			 	<div class="mask m3"></div>
			 	<div class="mask m4"></div>
			 	<div class="mask m5"></div>
			 	<div class="mask m6"></div>
				<div class="row">
					 <div class="col-md-12">			 	
					 	<div class="content">
					 		<div class="inner">
						 		<h1>Strategic Matchmaking</h1>
						 		<p class="large">We align your alliances strategy to your corporate plan, then act as matchmaker 
						 			and market scout. We help identify potential partners using a predefined set of criteria, make 
						 			initial introductions to possible game-changing partners, and you take it from there.</p>
						 		<ul class="large">
							 		<li>Aligning and setting the alliances strategy.</li>
									<li>Innovating together on possible opportunities.</li>
									<li>Mapping possible alliances across an ecosystem.</li>
									<li>Identifying possible alliance partners.</li>
									<li>Preparing approach.</li>
									<li>Matchmaking.</li>
						 		</ul>
					 		</div>
						</div>
						<blockquote class="small">
								  	<p class="large">"Martin developed the alliance strategy for Membership Rewards in Latin America. 
								  		Due to these efforts we were then able to develop a matrix of possibilities and prioritize based on 
								  		several criteria: need, what we had to offer, gap analysis, pricing, soft benefits and even brand 
								  		cachet. The strategy served as a global best practice and allowed us to have the richest Rewards 
								  		offering in international" </p>
									<footer class="large text-right"><cite title="Source Title"><span>Dan Austin</span> Former Cards Head 
										Latin America and Caribbean at American Express</cite></footer>
								</blockquote>
			     	</div>
			     	
			    </div><!--/.row-->
		    </div><!-- container ends-->
		</div>
		   
		<?php include './common/footer.php'; ?>
    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>