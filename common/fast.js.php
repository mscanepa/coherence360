<script src="./assets/js/jquery-1.11.2.min.js"  type="text/javascript"></script>
<script  type="text/javascript" src="./assets/js/jquery.mobile-1.4.5.min.js"></script>
<script src="./assets/js/bootstrap.min.js"></script>
<script src="./assets/js/fastClick.js"></script>
<script src="./assets/js/custom.js"  type="text/javascript"></script>
<script src="./assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

<?php	
	$pos = strripos ( $_SERVER['REQUEST_URI'] , "/" )+1;
	$dom= substr ( $_SERVER['REQUEST_URI'] , $pos);
?>

<script  type="text/javascript">
	$('a[href="<?=$dom;?>"]').addClass('active');
	$('a[href="<?=$dom;?>"]').parent("li").addClass('active');	
	$('a[href="<?=$dom;?>"]').parent("li").parent("ul").parent("li").addClass('active');
</script>

<script>
    $(function() {
        FastClick.attach(document.body);
    });
</script>