<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,300italic,300,100' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="./assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./assets/css/jquery.mobile-1.4.5.css" />
<link rel="stylesheet" type="text/css" href="./assets/css/styles.css?t=<?php echo time(); ?>" media="all" />
<link rel="stylesheet" href="./assets/css/jquery.mCustomScrollbar.css">
<script type="text/javascript">
    // first, create the object that contains
    // configuration variables
    MTIConfig = {};

    // next, add a variable that will control
    // whether or not FOUT will be prevented
    MTIConfig.EnableCustomFOUTHandler = true // true = prevent FOUT
</script>
<script type="text/javascript" src="http://fast.fonts.net/jsapi/3c1d79b4-941f-42c6-bbab-b1bf76f7fc1e.js"></script>

