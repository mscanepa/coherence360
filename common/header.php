	<div id="header" data-role="header" data-position="fixed" data-id="persistent" class="ui-header ui-bar-inherit ui-header-fixed slidedown">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-2">
					<div id="logo">
						<a href="./" title="Welcome to Coherence360" rel="external">
							<img src="./assets/images/logotipo-coherence.png" />
						</a>
					</div>
					<div class="icon-menu"></div>					
					
					<nav class="main-nav" class="anch">
						<div id="close-menu"></div>
						<ul class="l1">
						<li><div><a href="about-us.php" class="about.menu" rel="external">ABOUT</a></div>
							<ul>
								<li><a href="about-us.my-mission.php" rel="external">MISSION</a></li>
								<li class="last"><a href="about-us.martin-echavarria.php" rel="external">MARTIN ECHAVARRÍA</a></li>				
								<li class="last"><a href="about-us.associates.php" rel="external">ASSOCIATES</a></li>
                                <li class="last"><a href="about-us.experience.php" rel="external">EXPERIENCE</a></li>
							</ul>
						</li>
						<li><div><a href="framework.php" class="framework.menu"  rel="external">FRAMEWORK</a></div>
							<ul>
								<li><a href="framework.partnership.php" rel="external">OVERVIEW</a></li>
                                 <li><a href="framework.our-framework.php" rel="external">ALLIANCES FRAMEWORK</a></li>
								<li><a href="framework.our-approach.php" rel="external">OUR APPROACH</a></li>
                               
                                						
							</ul>
						</li>
						<li><div><a href="services.php" class="services.menu"  rel="external">SERVICES</a></div>
							<ul>
								
								<li><a href="services.partnerships-coaching-facilitation.php" rel="external">PARTNERSHIP FACILITATION</a></li>
								<li><a href="services.partnership-innovation-sessions.php" rel="external">PARTNERSHIP INNOVATION</a></li>
								<li><a href="services.strategic-matchmaking.php" rel="external">STRATEGIC MATCHMAKING</a></li>
								<li><a href="services.creating-alliances.php" rel="external">CREATING ALLIANCES</a></li>
								<li><a href="services.building-capability.php" rel="external">BUILDING CAPABILITY</a></li>						
							</ul>				
						</li>
						<li><div><a href="training.php" class="training.menu"rel="external">TRAINING</a></div>
                        	<ul>
								
								<li><a href="training.open-enrollment.php" rel="external">OPEN ENROLLMENT</a></li>
								<li><a href="training.close-enrollment.php" rel="external">TAILORED PROGRAMS</a></li>
														
							</ul>	
                        
                        </li>
						<li><div><a href="speaking.php" rel="external">SPEAKING</a></div></li>
						<li><div><a href="contact.php" class="contact.menu"  rel="external">CONTACT</a></div></li>
						<li class="clast"><div><a target="_blank" href="/praxis" rel="external">BLOG & RESOURCES</a></div></li>
						<li class="last"><div><a href="book.php" class="book.menu" rel="external">BOOK</a></div></li>
						
					</ul>
					</nav>
				</div>
	     	</div>
	    </div><!--/.row-->
	    <div class="hidden-rgt"></div>
    </div><!-- container ends-->
