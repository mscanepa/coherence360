<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="our-mission" class="about-us-2l neutro">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			<div class="mask m1"></div>
		 	<div class="mask m2"></div>
		 	<div class="mask m3"></div>
		 	<div class="mask m4"></div>
		 	<div class="mask m5"></div>
		 	<div class="mask m6"></div>
		 	<div class="mask m7"></div>
		 	<div class="mask m8"></div>
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
					 	<div class="col-md-12">
                            <img src="assets/images/martin.png" alt="Martin Echavarria" class="col-lg-4 pull-left" width="180px" style="padding:0 20px 0 0;">
                            	<h1 style="margin-top:0;">Martin Echavarría</h1>
					 			<p class="large">Martin Echavarría has dedicated his professional career in business collaboration, 
					 			the nexus where individuals and small groups come together to work productively. 
					 			From building Strategic Alliances and Partnerships across industries, culture, and geographies, 
					 			to coaching executives and their teams to collaborate effectively, Martin has successfully combined
					 			alliance development know-how with collaborative-leadership methodologies - enabling all kinds of 
					 			organizations to partner successfully.</p>
								<p class="large">He has over fifteen years' experience enabling strategic collaborations between global 
								companies resulting in over $2 billion in revenue. He has a Master's Degree in International Management 
								from Thunderbird School of Global Management, a Bachelor's Degree in Business and Political Science from 
								Emory University, studied Strategic Alliances at the Wharton School and Integral Coaching at New Ventures 
								West.</p>
								<p class="large">In 2012, Coherence received the Alliance Excellence Award from Association of 
								Strategic Alliances Professionals for the partnership between Scotiabank and Digicel, profiled by President 
								Bill Clinton in an article entitled "the Case for Optimism" in TIME Magazine. The alliance was the first 
								Mobile-Wallet in the Americas, allowing Haitians to access and transfer money via their cell phones.</p>
								<p class="large"><em>"What I enjoy most, is experiencing diverse groups come together in a moment of 
								accommodating illumination, where they set aside differences, and focus on the tasks they set before 
								themselves - building a partnership through their collaboration that results an alliance that 
								represents their concrete goals and aspirations."</em></p>
					 	</div>
					 </div>
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>