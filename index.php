<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>
</head>
<body id="page1" >
<div id="home" >
	<?php include './common/header.php'; ?>
	
	<div class="bd">		
		<div class="container">		
		<div class="next-page"><a href="about-us.php" data-transition="slide"></a></div>	
		<div class="row">
			 <div class="col-md-12">
			 	<div class="mask m1"></div>
			 	<div class="mask m2"></div>
			 	<div class="mask m3"></div>
			 	<div class="mask m4"></div>
			 	<div class="mask m5"></div>
			 	<div class="mask m6"></div>				 	
			 	<div class="slideshow ">
				 	<div class="slideshow-inner">		 		
			 			<div class="pre-cita">
							CREATE STRATEGIC
							PARTNERSHIPS &
							ALLIANCES TO
						</div>
				 		<div id="scroller" class="carousel slide">			 			
		        			<div class="carousel-inner">
		        			
		        				<div class="item active">	        				
			        				<div class="item-inner">
								 		<div id="cita-1" class="cita active">
											<strong>
												IGNITE
												COLLABORATIVE
												INNOVATION								
											</strong>
										</div>
									</div>
								</div>
								
								<div class="item">	        				
			        				<div class="item-inner">						
										<div id="cita-2" class="cita">
											<strong>
												GROW
												MARKET
												SHARE								
											</strong>
										</div>
									</div>
								</div>
								
								<div class="item">	        				
			        				<div class="item-inner">						
										<div id="cita-3" class="cita">
											<strong>
												ESTABLISH
												COMPETITIVE
												RESILIENCE								
											</strong>
										</div>
									</div>
								</div>
								
								<div class="item">	        				
			        				<div class="item-inner">						
										<div id="cita-4" class="cita">
											<strong>
												BUILD
												COLLABORATIVE
												ADVANTAGE								
											</strong>
										</div>
									</div>
								</div>
								
								<div class="item">	        				
			        				<div class="item-inner">						
										<div id="cita-5" class="cita">
											<strong>
												ENHANCE<br>
												CUSTOMER<br>
												VALUE								
											</strong>
										</div>
									</div>
								</div>
								<div class="item">	        				
			        				<div class="item-inner">						
										<div id="cita-5" class="cita">
											<strong>
												SUPPLEMENT
												CRITICAL <br>
												SKILLS								
											</strong>
										</div>
									</div>
								</div>		
								<div class="item">	        				
			        				<div class="item-inner">						
										<div id="cita-5" class="cita">
											<strong>
												DEVELOP 
												SUSTAINABLE 
												GROWTH								
											</strong>
										</div>
									</div>
								</div>								
							</div>
							
							<ol class="carousel-indicators">
							    <li data-target="#scroller" data-slide-to="0" class="active"></li>
							    <li data-target="#scroller" data-slide-to="1"></li>
							    <li data-target="#scroller" data-slide-to="2"></li>
							    <li data-target="#scroller" data-slide-to="3"></li>
							    <li data-target="#scroller" data-slide-to="4"></li>
							    <li data-target="#scroller" data-slide-to="5"></li>
                                <li data-target="#scroller" data-slide-to="6"></li>
                                
                                
							</ol>
							
						</div>
					</div>				
				</div>
	     	</div>
	     	
	    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>
    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>