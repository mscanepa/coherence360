<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="our-mission" class="about-us-2l trainingbg">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			<div class="mask m1"></div>
		 	<div class="mask m2"></div>
		 	<div class="mask m3"></div>
		 	<div class="mask m4"></div>
		 	<div class="mask m5"></div>
		 	<div class="mask m6"></div>
		 	<div class="mask m7"></div>
		 	<div class="mask m8"></div>
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
					 		<h1>Training</h1>
                            <p>Collaborative Leadership &#8211; is leadership that happens only at the group level and is foundational to organizational success. It is the capacity and capability of groups to collaborate as whole units of collective intention, emotion and action.</p>
                            <p>Enabling Collaborative Leadership &#8211; requires the support of a Partnership Coach to open group perspective by illuminating collective blind spots that help groups navigate the inherent challenges when they come together to work.</p>
                            <h2>Coherence training programs focus on two critical areas of learning:</h2>
                            <ol>
                            <li>Enabling Collaborative Leadership through Group Partnership Coaching and Facilitation Programs.</li>
                            <li>Enabling Strategic Alliances & Partnerships.</li>
                            </ol>
                            <h2>Fundamentals of Partnership Coaching &#8211; Two day learning program</h2>
                            <p>Working with yourself to build the capability to discern group interactions so that you can support the emergence of Collaborative Leadership.
Learn the Basics of the Operative Partnership Methodology that has been tested, is comprehensive, and can be adapted to any group partnering situation.
Become part of a community of practitioners and learners in the field.</p>
<h2>Enabling Successful Alliances and Partnerships the Fundamentals &#8211; Three day learning program</h2>
<p>This program incorporates the 5-Territories of Alliance Development correlated with the Operative Partnership Methodology, together this powerful learning experiences gives participants a framework that can be adapted to any group alliance development strategy.</p>
<p>Working with yourself to build the capability to discern group interactions so that you can support the emergence of Collaborative Leadership in the context of Alliances Building.
Learn the Basics of the Operative Partnership Methodology that has been tested, is comprehensive, and can be adapted to any group partnering situation and throughout the alliance development process.</p>
<p>Become part of a community of practitioners and learners in the field.</p>
					 		
                          
				 		</div>
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>