<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>

<div id="our-mission" class="about-us-2l neutro2 experience">
<?php include './common/header.php'; ?>
       <div class="bd">
              <div class="container">   
                     <div class="row">
                             <div class="col-md-12">                         
                                   <div class="content">
                                          <div class="inner ">
					 		<h1>Clients, Geographies, Industry & Types</h1>
                                                 <div class="row colOverview">
                                                        <div class="col-lg-3 ">
                                                               <h3>Client Short List:</h3>
                                                               <ul>
                                                               <li>American Express</li>
                                                               <li>Digicel Group</li>
                                                               <li>Hewlett Packard</li>
                                                               <li>Scotiabank International</li>
                                                               <li>Orange Telecom</li>
                                                               <li>PriceSmart</li>
                                                               <li>Verizon Wireless</li>
                                                               <li>Palacio de Hierro</li>
                                                               <li>Varig Airlines</li>
                                                               <li>Tam Airlines</li>
                                                               <li>Pao de Azucar</li>
                                                               <li>Lan Chile</li>
                                                               <li>Mexicana</li>
                                                               <li>And many more...</li>
                                                               </ul>
                                                        </div>
                                                        <div class="col-lg-3">
                                                               <h3>Geographies:</h3>
                                                               <ul>
                                                                      <li>Canada</li>
                                                                      <li>Caribbean Region</li>
                                                                      <li>Central America</li>
                                                                      <li>South America</li>                           
                                                                      <li>USA</li>
                                                               </ul>
                                                        </div>
                                                 <div class="col-lg-3">
                                                        <h3>Industries:</h3>
                                                        <ul>
                                                               <li>Airlines</li>
                                                               <li>Financial Services</li>
                                                               <li>Retail</li>
                                                               <li>Telecommunications</li>
                                                               <li>Supermarkets</li>
                                                        </ul>
                                                 </div>
                                                 <div class="col-lg-3">
                                                        <h3>Types of Partnerships:</h3>
                                                        <ul>
                                                               <li>Advertising</li>
                                                               <li>Branding</li>
                                                               <li>Channel</li>
                                                               <li>Licensing</li>
                                                               <li>Products & Services</li>
                                                        </ul>
                                                 </div>                          
				 		</div>
					</div>
		     	      </div>
                     </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>