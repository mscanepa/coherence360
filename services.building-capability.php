<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="parterships-coaching-facilitation" class="services-2l">	
		<?php include './common/header.php'; ?>
		
		<div class="bd">
			<div class="container">
				<div class="mask m1"></div>
			 	<div class="mask m2"></div>
			 	<div class="mask m3"></div>
			 	<div class="mask m4"></div>
			 	<div class="mask m5"></div>
			 	<div class="mask m6"></div>
				<div class="row">
					 <div class="col-md-12">			 	
					 	<div class="content">
					 		<div class="inner">
						 		<h1>Building Capability</h1>
						 		<p class="short">We help you establish an Alliance Business Capability, with all the requisite tools, methods and capabilities to build strategic alliances and partnerships.</p>
				 		
						 		<ul class="short">
							 		<li>Integrating alliances strategy into corporate strategy.</li>
									<li>Building the competency model.</li>
									<li>Developing alliance business organizational structure.</li>
									<li>Training alliance teams.</li>
									<li>Building alliance development tools.</li>
									<li>Enable the emergence of a collaborative advantage.</li>
						 		</ul>
						 	</div>
					 		
						</div>
			     	</div>
			     	
			    </div><!--/.row-->
		    </div><!-- container ends-->
		</div>
		   
		<?php include './common/footer.php'; ?>
    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>