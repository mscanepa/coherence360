<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body id="page4">


<div id="services"  data-role="page">	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			<div class="prev-page"><a href="resourses.php" data-transition="slide"  data-direction="reverse" ></a></div>
			<div class="next-page"><a href="./" data-transition="slide"></a></div>
			<div class="row">
				 <div class="col-md-12">				 	
				 	<div class="slideshow ">
					 	<div class="slideshow-inner sttc">
					 		<div class="cita">
					 			<div class="cita-inner">
					 				<div class="cita-content">
							 			BOOK
							 		</div>
					 			</div>
					 		</div>
					 		<div class="desc">
					 			<div class="desc-inner">
					 				<div class="desc-content">
										<p>
										Coherence works with organizations to conceive and create game changing strategic collaborations.  Through the applied consulting model, long term business relationships are developed. 
										<br /><br />
										Being results-driven and committed to collaboration I work with clients to unlock the full potential of partnership opportunities that can be created between companies, across industries, geographies and cultures. 
										<br /><br />
										As Partnership Development Coach, I advocates objectively for the good of the strategic alliance formed between firms so that it brings value to both - or in the case of more complex alliance networks, value to all involved.
										
										</p>
									</div>
								</div>						
							</div>
						</div>
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	     </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>   
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>