<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<style>

.contact.menu .ui-link{
	color: #58595b;
	border-bottom: 3px solid #fff !important;
}


</style>
<body>


<div id="overview" class="framework-2l">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
                        <h1 class="multicolum">Overview</h1>
                        	<div class="row colOverview">
							  	<div class="col-sm-12 col-md-6 ">
							  		<p>Coherence's Alliance Framework is founded on five core concepts:</p>
                            		<p>The first is that <em>collaborative leadership</em> is foundational for alliance success. It occurs only when individuals come together to work as a <em>group, and never only at the individual level</em>. A successful alliance is therefore predicated on groups working collaboratively together.</p>
                            		<p>The second: collaborative leadership and the capacity and capability to move into collaboration are fundamentally an emotional group process of relatedness, belonging and mutuality - which are developed over time and are foundational to building trust.</p>
                            		<p>Third: alliances and partnerships can only move beyond situational problem solving towards a collective orientation to co-create a desired future when group collaboration emerges.</p>
                            	</div>
							  	<div class="col-sm-12 col-md-6">
							  		<p>Fourth: the partnership journey toward collaboration entails more than just learning.  It requires experience that goes beyond learning and encompasses working, winning and facing challenges together.  Building alliances across complex and varies business processes requires collaborative experiential learning.</p>
                            		<p>Finally: both or all parties involved in a partnership require guidance from an objective <strong><em>Partnership Facilitator & Coach</em></strong> whose role is to support the <em>partnership</em> (distinct from the parties in it).  The partnership coach provides perspective by illuminating collective blind spots, and is incentivized by overall partnership success.  This is essential to help navigate and overcome the many challenges which always arise when groups of people attempt to collaborate toward complementary or supplementary objectives.</p>
							  	</div>
							</div>
						</div>
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>