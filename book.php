<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="our-mission" class="framework-2l">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 			<div class="inner ">
                           
                         <div class="pull-left col-lg-8 multicolum">
                         <img src="assets/images/book.png" width="80px" alt="Enabling Collaboration" class="pull-left">
                         <div class="pull-left" style="padding-left:10px;">
                        	<h1>Enabling Collaboration</h1>
                            <h6><em>A Framework for Successfully Establishing<br> Strategic Alliances and Partnerships</em></h6>
                            <br>
                         </div>
                         </div>
                         <div class="clearfix"></div>
                        <div class="multicolumna2columnas">
					 	

                      
					 		
					 		<p>To be published by LID Publishing in 2015.</p>
                                <p>In the new millennium, business collaboration is the name of the game. Gone are the days when a singular company that housed all its value creating activities under one roof could succeed. The fact is that companies of all sizes now focus on their core competencies and work with a global network of strategic suppliers and partners to bring products and services consistently to market. In 1980, only 2% of corporate value was tied into some kind of collaborative venture. Today studies show that as much as 25% of corporate revenues and value are tied to alliances. This amounts to trillions of dollars built around business collaboration. From the smallest firms to the largest enterprise companies, everyone is waking up to the fact that the world has become a complex web of interdependent actors—some are competing, while others are learning to collaborate in new and innovative ways to create exponential value.</p>
                                <p><em>Enabling Collaboration</em> offers executives a practical, accessible approach to building real-world business partnerships that marry effective and innovative group-leadership practices with a meta-process for building a successful alliance. The methods described can be applied no matter the industry or the size of the partnering organizations. <em>Enabling Collaboration </em>is a must-read for any corporate innovator who is interested in driving tremendous value and success for themselves and their companies in this new world of global business alliances.</p>
                                <p><em>Enabling Collaboration</em> views the process of effective group leadership and alliances collaboration as one and the same. Both must take place concurrently for alliances between firms of all sizes, of different cultures, and of varied industries to be successful. Group leadership is not something done in limited offsite retreats with colleagues. Rather, it is a systematic activity that occurs from meeting to meeting and across a business process through every business interaction. This results in self-correcting, self-generating partnerships that evolve and sustain themselves over time. As a result, readers of Enabling Collaboration will learn a new standard of business practice they can apply to be successful.</p>
                                <p>Under this system, partnerships and alliances are created through a process where the capacities and capabilities of the individuals and sub-groups in the partnering firms coalesce to create a unique third entity-The Alliance Group. This entity is reflective of both the companies and of the individuals who participate in the alliance building. The relationships within this unique entity are established based on contextual challenges, opportunities, and the wants and needs inherent in the industries, businesses, countries and economies in which the companies are embedded. This is a holistic, systems-based approach.
</p>
       </div>
				 		</div>
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>