<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body id="page4">


<div id="services"  data-role="page">	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			<div class="next-page"><a href="training.php" data-transition="slide"></a></div>
			<div class="prev-page"><a href="framework.php" data-transition="slide"  data-direction="reverse" ></a></div>
			<div class="row">
				 <div class="col-md-12">				 	
				 	<div class="slideshow ">
					 	<div class="slideshow-inner sttc">
					 		<div class="cita">
					 			<div class="cita-inner">
					 				<div class="cita-content">
							 			BUILDING GAME-CHANGING <br />
							 			<strong>
							 			STRATEGIC COLLABORATIONS
							 			</strong>
							 		</div>
					 			</div>
					 		</div>
					 		<div class="desc">
					 			<div class="desc-inner">
					 				<div class="desc-content">
										<p>As <em>Partnership Development Coach & Facilitator</em>  we work with organizations to conceive and create game changing strategic collaborations.  </p>
										<p>Being results-driven and committed to collaboration, we work with clients through an applied consulting approach to unlock the full potential of partnership opportunities across industries, geographies and cultures.</p>
										<p>We advocate objectively for the good of the potential strategic alliance between firms, so they can overcome obstacles to collaboration and discover value to both - or in the case of more complex alliance networks, value to all involved.									
										</p>
									</div>
								</div>						
							</div>
						</div>
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	     </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>   
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>