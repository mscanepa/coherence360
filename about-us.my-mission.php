<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="our-mission" class="about-us-2l ">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			<div class="mask m1"></div>
		 	<div class="mask m2"></div>
		 	<div class="mask m3"></div>
		 	<div class="mask m4"></div>
		 	<div class="mask m5"></div>
		 	<div class="mask m6"></div>
		 	<div class="mask m7"></div>
		 	<div class="mask m8"></div>
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
				 			 <div class="col-md-12">	
					 		<h1>Mission</h1>
					 		<p class="short">
								Our mission is to advance collaborative business practices that help companies and organizations to become more innovative, creative and resilient through the strategic partnerships and alliances they form.
                             </p>
                             <p class="short">
                                
								When mission achieved, the Partnerships and Alliances formed become self-generating and self-correcting. In this way, they have the capability to adapt to market changes while also develop opportunities unseen in their initial launching.
					 		
					 		
					 		</p>
					 	</div>
				 		</div>
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>