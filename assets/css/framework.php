<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body id="page3">


<div id="framework" data-role="page">
	
	<?php include './common/header.php'; ?>
	
	<div role="main" class="bd ui-content">
		<div class="container">			
			<div class="prev-page"><a href="about-us.php" data-transition="slide"  data-direction="reverse" ></a></div>
			<div class="next-page"><a href="services.php" data-transition="slide"></a></div>
			<div class="row">				
				 <div class="col-md-12">
				 	<div class="slideshow ">
					 	<div class="slideshow-inner sttc">
					 		<div class="cita">
					 			<div class="cita-inner">
					 				<div class="cita-content">
							 			To improve
							 			success rates
							 			we employ an
							 			
							 			<strong>
								 			Integrated
							 			Framework
							 			IS EMPLOYED					 			
							 			</strong>
							 		</div>
					 			</div>
				 			</div>
				 			<div class="desc">
				 				<div class="desc-inner">
				 					<div class="desc-content">
									<p>
									Like a jazz ensemble, enabling alliances teams to play together well is critical so they can establish an alliance that truly succeeds over time. 
									</p>
									<p>
									The proven Alliances Framework combines a collaborative leadership methodology with a unique alliance development process � bridging a critical gap for sustained success, a group leadership and a business process specifically engineered for establishing strategic business relationships.
									
									</p>
								</div>
								</div>
							</div>						
						</div>	
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>