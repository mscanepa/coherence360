var cita="cita-1";
$(document).ready(function(){
		
		var time = 250;
		var dsk=1;
			
		/*dsk= 0;
		if($(".slideshow.sttc").html()!=undefined ){			
			$('.slideshow').animate({"margin-left":"688px"}, time*10*dsk, "swing", function() {					
				
			});
		}*/			

		if($(".about-us-2l").html()!=undefined){			
			if($(window).width()<=768){
				dsk=0;			
			}
			time=500;
			
				setTimeout(function(){
					$('.mask.m8').css('display','block');			
				}, time*1*dsk);
				
				setTimeout(function(){
					$('.mask.m7').css('display','block');
				}, time*2*dsk);
				
				setTimeout(function(){
					$('.mask.m6').css('display','block');
				}, time*3*dsk);
				
				setTimeout(function(){
					$('.mask.m5').css('display','block');
				}, time*4*dsk);
				
				setTimeout(function(){
					$('.mask.m4').css('display','block');
				}, time*5*dsk);
				
				setTimeout(function(){
					$('.mask.m3').css('display','block');
				}, time*6*dsk);
				
				setTimeout(function(){
					$('.mask.m2').css('display','block');
				}, time*7*dsk);
				
				setTimeout(function(){
					$('.mask.m1').css('display','block');
				}, time*8*dsk);
		}
	
		if($(".services-2l").html()!=undefined){			
			if($(window).width()<=768){
				dsk=0;			
			}
			
				setTimeout(function(){
					$('.mask.m6').css('display','block');			
				}, time*1*dsk);
				
				setTimeout(function(){
					$('.mask.m5').css('display','block');
				}, time*2*dsk);
				
				setTimeout(function(){
					$('.mask.m4').css('display','block');
				}, time*3*dsk);
				
				setTimeout(function(){
					$('.mask.m3').css('display','block');
				}, time*4*dsk);
				
				setTimeout(function(){
					$('.mask.m2').css('display','block');
				}, time*5*dsk);
				
				setTimeout(function(){
					$('.mask.m1').css('display','block');
				}, time*6*dsk);
		}
		
		$(document).on('pagebeforeshow', '#home, #framework, #about-us, #services, #training', function(event){
			$(".slideshow-inner.sttc .desc-content").mCustomScrollbar({
				theme:"light-thick"
			});
			
			fitBd();
			fitPage();
		});
	
		$(document).on('pageshow', '#framework, #about-us, #services', function(event){				 
			$.createHoverEvents();				
		});
	
		$(document).on('pageshow', '#home', function(event){			
			$.showFibonacciHome();
			$.createHoverEvents();			
			
		});
		 
		$.showFibonacciHome = function(){				 
			if($(window).width()<=768){
				dsk=0;			
			}
		
			setTimeout(function(){
				$('.mask.m1').css('display','none');			
			}, time*1*dsk);
			
			setTimeout(function(){
				$('.mask.m2').css('display','none');
			}, time*2*dsk);
			
			setTimeout(function(){
				$('.mask.m3').css('display','none');
			}, time*3*dsk);
			
			setTimeout(function(){
				$('.mask.m4').css('display','none');
			}, time*4*dsk);
			
			setTimeout(function(){
				$('.mask.m5').css('display','none');
			}, time*5*dsk);
			
			setTimeout(function(){
				$('.mask.m6').css('display','none');
			}, time*6*dsk);
			
			setTimeout(function(){
				$('.slideshow-inner').animate({"margin-left":"688px"}, time*10*dsk, "swing", function() {					
					$('#scroller.carousel').carousel({
						interval: 4000
					});
					$('#home .prev-page, #home .next-page').css('display', 'block');
				});
			}, time*7*dsk);
			
		}
		 
		$.createHoverEvents = function(){
			/*$('.prev-page, .next-page').hover(function(){
				$(this).children('a').trigger("click");
			 });*/
		}

		if($("#home").html()!=undefined){
			$.showFibonacciHome();
		}
		
		$.createHoverEvents();	 
		 
		fitBd();		   
		fitPage();
		fixCircle();
   
		$(window).resize(function (){
			fitBd();
			fixCircle();
		});
		
		$(".slideshow-inner.sttc .desc-content").mCustomScrollbar({
			theme:"light-thick"
		});

		$(".trainingbg .content").mCustomScrollbar({
			theme:"dark-thick"
		});
		
		$(".services-2l .content").mCustomScrollbar({
			theme:"dark-thick"
		});
		
		$(".about-us-2l .content").mCustomScrollbar({
			theme:"dark-thick"
		});
		
		$(".framework-2l .content").mCustomScrollbar({
			theme:"dark-thick"
		});
		
		$('.main-nav ul li div, .main-nav ul li ul').mouseover(function(){
			if(!$(this).parent('li').hasClass('active')){
				$('nav.main-nav ul li.active ul').css('display','none');
			}			
		});		
		
		$('.main-nav ul li div, .main-nav ul li ul').mouseout(function(){
			$('nav.main-nav ul li.active ul').css('display','block');

			
		});		
			
});

$(document).on("pageinit", function () {
	$('.icon-menu').click(function(){
		if($('.main-nav ul.l1').hasClass("open1")){
			$.closeMenu();
		}else{
			$.openMenu();
		}
	});
	
	$.openMenu = function()
	{
		$('.main-nav ul.l1').css('display', 'block');
		$('.main-nav ul.l1').animate({opacity: '1'}, 500);
		$('.main-nav ul.l1').addClass("open1");
	};

	$.closeMenu = function()
	{
		$('.main-nav ul.l1').animate({opacity: '0'}, 500);
		$('.main-nav ul.l1').css('display', 'none');
		$('.main-nav ul.l1').removeClass("open1");
	};
	
	
});

$(document).bind("mobileinit", function(){/*
    $.mobile.defaultPageTransition = 'none'; 
    $.mobile.pushStateEnabled = false;  
    $.mobile.transitionFallbacks.slideout = "none";*/
  });

function fitPage(){
	var w = $(window).width();
	if(w<=768){
		$('meta[name=viewport]').attr('content','width=480, initial-scale='+((w/480)).toFixed(2)+' , user-scalable=no');
		//$('meta[name=viewport]').attr('content','width=device-width, initial-scale=1 , user-scalable=no');
	}else if(w<=1024){
		$('meta[name=viewport]').attr('content','width=1024, initial-scale='+((w/1024)).toFixed(2)+' , user-scalable=no');
	}
}

function fitBd(){
	var w = $(window).width();
	var h= $(window).height();
	var nh= h - 84 - 35 - 4;
	  
	   
	if(h>550 && h<768){
		 $('.bd').css('height', nh+'px' );
	}else if(h>=768){
		 $('.bd').css('height', '648px' );
		 var x = h- (648+85+30+6);
		 $('.footer').css('bottom', x+'px' );
	}else if(h<600 && w<768){
		$('.bd').css('height', '500px' );
	}else if(h<550 && w>=768){
		$('.bd').css('height', '427px' );
	}
	
	var nw = w - 1024;
	if(w>1024){
		$('#header .hidden-rgt').css('width', nw+'px' );
		$('#header .hidden-rgt').css('display', 'block' )
	}else{
		$('#header .hidden-rgt').css('display', 'none' )
	}
}

function fixCircle(){
	$('.circle').each(function() {
	    var $w = $(this).width();
	    $(this).css('height', $w+'px')
	});
}

