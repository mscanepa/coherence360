	<div id="footer" data-role="footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 col-md-8">
					<div id="copy">
						&copy; 2015. All rights reserved.
					</div>
				</div>
	     		<div class="col-xs-6 col-md-4">
					<div id="ebc">
						Enabling Business Collaboration
					</div>
				</div>
	     	</div>
	    </div><!--/.row-->
    </div><!-- container ends-->
