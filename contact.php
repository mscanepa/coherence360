<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">	
</head>
<style>
a.link
	{
		color:#999;
		}
a.link:hover
	{
		color:#000;
		}
</style>
<body>


<div id="our-mission" class="about-us-2l contactbg">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			<div class="mask m1"></div>
		 	<div class="mask m2"></div>
		 	<div class="mask m3"></div>
		 	<div class="mask m4"></div>
		 	<div class="mask m5"></div>
		 	<div class="mask m6"></div>
		 	<div class="mask m7"></div>
		 	<div class="mask m8"></div>
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
				 			<div class="col-md-12">
				 				<blockquote>
								  	<p class="short">"Business is a critical enabler to inclusive growth and job creation which is key to unlocking human potential." </p>
									<footer class="short text-right"><cite title="Source Title">Paul Polman Global Unilever Chief</cite></footer>
								</blockquote>
					 		
                           <p><address><strong>Coherence Inc.</strong><br>9 Island Avenue, 1010<br>
                           Miami Beach, 33139</address>
                           </p>
                             <p class="contactp">To learn more about our products <br>																																						& services or attend an event:<br>
                         
                           <strong>Email:</strong> <a class="link" href="mailto:learnmore@coherence360.com">learnmore@coherence360.com</a><br>
                          <strong> Call:</strong> 305-446-5858</p>
                        																																																			
                          																																																													
						
                        <p>To connect socially visit: </p>
                        <p  class="contactp">
                        <a class="link" href="https://www.facebook.com/martin.echavarria1?fref=ts"> <i class="fa fa-facebook pull-left"></i></a>
                        <a class="link" href="https://twitter.com/Coherence360"> <i class="fa fa-twitter pull-left"></i></a>
                        <a class="link" href="https://www.linkedin.com/company/302281?goback=%2Efcs_GLHD_coherence_false_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2&trk=ncsrch_hits"> <i class="fa fa-linkedin-square fa-1 pull-left"></i></a>
                        <div class="clearfix"></div>
                        
                      </div>
                            
					 		
			 		  </div>
                     
                       
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>