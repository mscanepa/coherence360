<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<style>

.contact.menu .ui-link{
	color: #58595b;
	border-bottom: 3px solid #fff !important;
}


</style>
<body>


<div id="overview" class="framework-2l">
	
	<?php include './common/header.php'; ?>
	
	<div class="bd">
		<div class="container">
			
			<div class="row">
				 <div class="col-md-12">			 	
				 	<div class="content">
				 		<div class="inner">
                        <h1 class="multicolum">Speaker's Bio: Martín Echavarria</h1>
                        	<div class="row colOverview">
							  	<div class="col-sm-12 col-md-6 ">
							 		<img src="assets/images/martin_speaking.jpg" alt="Martin Echavarria Speaking" class="col-lg-8 pull-left" 
		                            width="400px" style="padding:0 20px 0 0;">		
									<p>Martín Echavarria is a pragmatic, passionately engaged speaker that delivers 
					 				illuminating and thoughtful insights challenging us all to rethink 
					 				collaboration. His real and down-to-earth conversational approach conveys an 
					 				optimism that is grounded in intellectual insights which invite audiences to 
					 				co-think and co-create together.</p>
					 				<p>As a developer of Strategic Alliances and Partnerships, 
				 					and a Leadership Coach and Team Facilitator to the C-Suite, he offers a fresh 
				 					view of the ‘what’ and ‘how’ of collaboration.</p>
					 				<p>Indeed, collaboration is the buzzword of this century and yet, 
				 					few truly understand how it emerges, how it can be enabled and what it means 
				 					to us all. After all, companies and organizations of all kinds rely on teams 
				 					collaborating together internally, as well as groups working across 
				 					organizations on a daily basis to make business a reality.</p>
									<p>For Martín, today’s organizational success is all about 
									Collaborative Leadership; a capability that must be mastered in order for 
									collaborative advantage to take hold and corporate resilience to be developed. </p>
									<p>His talks enlighten and enliven audiences on how to make this a 
									reality. How we can be more connected to our work, and more empowered to face 
									the opportunities and challenges of today, through the collaborative 
									relationships we co-create ‘with’ and ‘through’ others.</p>
                            	</div>
							  	<div class="col-sm-12 col-md-6">
							  		
								<p>Dedicated to business collaboration, through his consulting work 
									in Strategic Alliances and Partnerships at Coherence Inc. and as an Executive 
									Leadership Coach and Team Facilitator through Korn/Ferry and Executive Core, 
									Martín has collaborated with companies like Verizon, Hewlett-Packard, American 
									Express, MasterCard, Scotiabank, PriceSmart, Digicel, Orange Telecom, Bunge 
									and many others. He has spoken on alliances through the Association of 
									Strategic Alliances Professionals (ASAP) and has taught collaboration to 
									Executive MBA Students. </p>
								<p>He attained a Master’s Degree in International Management from 
									Thunderbird School of Global Management, a Bachelor’s Degree in Business and 
									Political Science from Emory University, studied Strategic Alliances at the 
									Wharton School and Integral Coaching at New Ventures West.</p>
								<p>All talks can be delivered in English and Spanish and are 
									tailored to audiences and the challenges they face.</p>
								<p>Example Speaking Headliners:
									<ul>
										<li>Collaborative Leadership in the New Millennium – the Missing Link to 
										meeting today’s global opportunities and challenges.</li>
										<li>Sustainability and the Circular Economy – Opportunities in Collaboration 
										throughout Supply & Value Chains.</li>
										<li>International Strategic Alliances & Partnerships – Enabling Collaboration across cultures, 
										industries and geographies.</li>
										<li>Building Collaborative Advantage – The New Playing Field for Competitive Resilience.</li>
										<li>Collaborative Negotiations – The Co-creation Model of Negotiating Partnerships & Alliances.</li>
										<li>Collaborative Innovation – Mastering the Power of Working-Together.</li>
									</ul>
					 			</p>	 		
							  	</div>
							</div>
						</div>
					</div>
		     	</div>
		     	
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>