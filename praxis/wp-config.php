<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('WP_MEMORY_LIMIT', '64M');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'praxis');

/** MySQL database username */
define('DB_USER', 'praxiscoherence');

/** MySQL database password */
define('DB_PASSWORD', 'YUvMcqtqFH9Cg7jg');

/** MySQL hostname */
define('DB_HOST', '205.178.146.115');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b.+N7,/L!4m-){-RGduahJ9I`02SMGehc@ pc.Ocm*]@3)9Uw/f>Z19I+8S1z=Ee');
define('SECURE_AUTH_KEY',  '>|NL-zBPLr-i9fKI5~b]UnQ;[pY+moc2t!5E*6R1KiFu>y-|SxUOwi7]/n$F#yEv');
define('LOGGED_IN_KEY',    '$iN!D`TWJ:9qgK0;+nYY`}l]Atdgu<#xls8LklU0C|*|i|;Pk/Y(;W9uJ+|ku~BM');
define('NONCE_KEY',        'Qf4jQa*U(zV|-%|;3r+x6]hfS;dtcdiv2RY*:$5=tm *LF8j7Qh/E5>OSDi%HYZ-');
define('AUTH_SALT',        '~%>VyL8VqeI~;)&J$p(dGYjYE6?uI{g!FU;{mZ484-)2r66+-e&caDUdj#|!u]#r');
define('SECURE_AUTH_SALT', '>V3 ,s(qhRs7K7:|jCl;]0%+6<66E$gqk0;: -?=64%MP41qT3XDxjwYj+f{~L-h');
define('LOGGED_IN_SALT',   '[4 [,)Eqh<=mG-`g|.P|Z1eN(<cR6-VFi};@+`Vie{^cn1xsIu}+2Er[q]?ABV,U');
define('NONCE_SALT',       '14!Og6sIaTU0t-D?+@w6rri;XF&3@3Fu;|CVH//-+p-)=uTsTAE|WK8XxT@Dp^[2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
