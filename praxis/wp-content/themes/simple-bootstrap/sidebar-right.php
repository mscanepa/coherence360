
<?php if ( is_active_sidebar( 'sidebar-right' ) ) { ?>
<div id="sidebar-right" class="<?php simple_boostrap_sidebar_right_classes(); ?>" role="complementary">
	<div id="textSidebar">
		<h2>About Praxis</h2>
		<p><strong>Praxis</strong> is the process by which a theory, lesson, or skill is enacted, embodied, and realized through engagement, application, exercise, and practice.</p>
		<p>Here at <strong>PRAXIS</strong>, the <em>Journal of Applied Collaborative Leadership</em>, practitioners contribute, share and learn together to develop <strong>PRAXIS</strong> in enabling collaboration to emerge in all areas of organizational life.</p>
	</div>
    <div class="vertical-nav block">
    <?php dynamic_sidebar( 'sidebar-right' ); ?>
    </div>
 </div>
<?php } ?>