<form action="<?php echo home_url( '/' ); ?>" method="get">
    <fieldset>
		<div class="input-group">
			<input type="text" name="s" id="search" placeholder="Search by" value="<?php the_search_query(); ?>" class="form-control" />
				<button type="submit" class="btn"><i class="icon-caret-down"></i></button>
		</div>
    </fieldset>
</form>