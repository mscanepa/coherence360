<!doctype html>  

<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<script type="text/javascript" src="http://fast.fonts.net/jsapi/3c1d79b4-941f-42c6-bbab-b1bf76f7fc1e.js"></script>

	<?php wp_head(); ?>
	<?php comments_popup_script(600, 500); ?>
</head>
	
<body <?php body_class(); ?>>
<header class="subscribe">
	<div class="col-md-4">
<?php
        // get our subscription form
        $subscriptionForm = \SimpleSubscribe\Developers::getSubscriptionForm();
        // with this we determine modal windows class, since it's hidden automatically,
        // with every submission, we should make it visible, therefore add class "visible"
        $modalWindowVisible = $subscriptionForm->isSubmitted() ? 'visible' : '';
        // just empty variable to be filled with errors / success message
        $subscriptionMessage = '';
        // is it valid or not?
        if($subscriptionForm->isSubmitted() && $subscriptionForm->isValid()){
            // it is, this is our messages
            $subscriptionMessage = 'You have succesfully subscribed, e-mail is on it\'s way!';
        } elseif($subscriptionForm->isSubmitted() && $subscriptionForm->hasErrors()) {
            // it's not! get error messages in variable
            $subscriptionMessage = print_r($subscriptionForm->getAllErrors(), TRUE);
        }
    ?>
        <?php
            // print message, if any
            echo $subscriptionMessage;
            // display form
            echo $subscriptionForm;
        ?>
	</div>
</header>
	<div id="content-wrapper">

		<header>
			<nav class="navbar navbar-default navbar-static-top">
				<div class="container">
		  
					<div class="header">
						<?php if (has_nav_menu("main_nav")): ?>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-responsive-collapse">
		    				<span class="sr-only"><?php _e('Navigation', 'simple-bootstrap'); ?></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<?php endif ?>
						<a class="navbar-brand" title="<?php bloginfo('description'); ?>" href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo esc_url(home_url('/')); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>"/></a>
					</div>

					<?php if (has_nav_menu("main_nav")): ?>
					<div id="navbar-responsive-collapse" class="collapse navbar-collapse">
						<?php
						    simple_bootstrap_display_main_menu();
						?>

					</div>
					<?php endif ?>

				</div>
			</nav>
		</header>
		
		<div id="page-content">
			<div class="container">
