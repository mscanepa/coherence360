# Mooberry Book Manager
**Contributors:** mooberrydreams  
**Tags:** book, author, publisher, writer, books, writing, publishing, authors   
**Requires at least:** 3.8.0  
**Tested up to:** 4.2  
**Stable tag:** 2.0  
**License:** GPLv2 or later  
**License URI:** http://www.gnu.org/licenses/gpl-2.0.html  

## Description
An easy-to-use system for authors. Add your new book to your site in less than 5 minutes.


No coding is necessary to use Mooberry Book Manager. Adding books is as easy as filling out a form. Include as much or as little information as you want, including the book cover, links to purchase the book, reviews of the book, an excerpt, and more.

Mooberry Book Manager will create a page for each book, ensuring a consistent look on all of your pages.

Organize your book into grids with just a few clicks. Grids can include all of your books or a selection of books, and you choose how they are ordered. You can create multiple grids; for example, an "Available Now" page and a "Coming Soon" page. Grids update automatically when you edit or add books. Set it and forget it!

Feature books on your sidebar with four options:
* A random book  
* The newest book  
* A book that's coming soon  
* A specific book  
	
Mooberry Book Manager works with your chosen theme to provide a consistent look throughout your website.

Requires Wordpress 3.8. The admin screens (for creating books, etc.) require Javascript, but the public pages do not.

Want regular updates? 
* Like Mooberry Dreams on Facebook: https://www.facebook.com/MooberryDreams
* Follow Mooberry Dreams on Twitter: https://twitter.com/MooberryDreams
* Check out the blog: http://www.mooberrydreams.com/blog
* Subscribe to Mooberry Dreams' mailing list: http://www.mooberrydreams.com/products/mooberry-book-manager/

Additonal questions?

Download the [User Manual](http://www.mooberrydreams.com/support/mooberry-book-manager-support/) (PDF format)


## Changelog
#### 2.0 
* New: Added General Settings, Publisher Settings, and Edition Formats Settings
* New: Added Editions to book pages 
* New: Added Librarian and Master Librarian Roles
* New: Added French translation
* Improved: Redesigned layout of Book Page
* Improved: Blank data defaults and labels no longer display on Book Page
* Improved: Added Comments support to books
* Fixed: Moved tags to new custom taxonomy instead of using post tags.

#### 1.3.2   
* Added 9 new retailers   
* Escapes add_query_vars for safety   

#### 1.2 
* Fixes bugs introduced in 1.1 (oops!)

#### 1.1 
* Added support for language translations   
* Added German translation  
* Fixed issues resulting from Customizr theme  
* Fixed bug with book drop down in widget  

#### 1.0 

* Initial Release

