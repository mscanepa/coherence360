<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="partership-coaching-facilitation" class="services-2l">	
		<?php include './common/header.php'; ?>
		
		<div class="bd">
			<div class="container">
				<div class="mask m1"></div>
			 	<div class="mask m2"></div>
			 	<div class="mask m3"></div>
			 	<div class="mask m4"></div>
			 	<div class="mask m5"></div>
			 	<div class="mask m6"></div>
				<div class="row">
					 <div class="col-md-12">			 	
					 	<div class="content">
					 		<div class="inner">
						 		<h1>Partnership Innovation Sessions</h1>
						 		<p class="large">These are specifically designed and facilitated work sessions where alliances teams 
						 		co-create and co-discover what their alliance could look like. These multi- stakeholder meetings typically 
						 		involve 10 to 25 people from marketing, operations, finance and other areas of the participating companies 
						 		and is a foundational step to forming the partnership. The resulting alliance design is then further solidified 
						 		through Territory Three, where ideas are tested in the market and the alliances structure is consolidated.</p>
                               <p class="large">For underperforming alliances, Partnership Innovation Sessions are an essential step to help them get back on track. Partnership Innovation Sessions help to:</p>
						 		</p>
						 		<ul class="large">
							 		<li>Build the collective vision of a major strategic collaboration.</li>
									<li>Map the potential of an alliance across business units, products and services that
									results in innovative ideas that emerge from group collaboration.</li>
									<li>Get underperforming partnerships back on track.</li>
									<li>Establish the real-shared potential of a current or potential strategic alliance.</li>
									<li>Optimize current alliances to the next level of operability.</li>
						 		</ul>
					 		</div>
						</div>
			     	</div>
			     	
			    </div><!--/.row-->
		    </div><!-- container ends-->
		</div>
		   
		<?php include './common/footer.php'; ?>
    
</div>
<?php include './common/fast.js.php'; ?>

 </body>
</html>