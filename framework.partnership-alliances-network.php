<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include './common/head-tag.php'; ?>	
</head>
<body>


<div id="framework">
	
	<?php include './common/header.php'; ?>
	
	<div id="bd">
		<div class="container">			
			<div class="row">
				 <div class="col-md-6">			 	
				 	<div class="content">
				 		<h1>Overview</h1>
                        <h2>Coherence’s Alliance Framework is founded on five core concepts:</h2> 		
						<p>The first is that <em>collaborative leadership</em> is foundational for alliance success. It occurs only when individuals come together to work as a <em>group, and never only at the individual level</em>. A successful alliance is therefore predicated on groups working collaboratively together.</p>			 		
					</div>
		     	</div>
		     	<div class="col-md-6">			 	
				 	<div class="content">
						<p>
							The 5-Territories of Alliance Development are a roadmap for establishing successful strategic alliances. In each territory, groups work on a set of business activities that are particular to where they are in the process. Each territory naturally transitions to the other through a set of gates and group leadership challenges that teams must traverse.
							The Operative Partnership Methodology is a system of methods that can be applied along the 5-Territories of Alliance Development. The methodology uses a particular set of tools, group coaching and facilitation practices that support an alliance’s teams to collaborate together and overcome group leadership challenges as they form the alliance.
						</p>			 		
					</div>
		     	</div>
		    </div><!--/.row-->
	    </div><!-- container ends-->
	</div>
	   
	<?php include './common/footer.php'; ?>
    
</div>

<?php include './common/fast.js.php'; ?>

 </body>
</html>